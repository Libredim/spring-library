package com.library;

import com.library.controller.dto.ReturnBookInput;
import com.library.model.dao.entity.Abonnement;
import com.library.model.dao.entity.Book;
import com.library.model.dao.entity.User;
import com.library.model.dao.jpa.BookRepository;
import com.library.model.dao.jpa.UserRepository;
import com.library.model.dao.service.AbonnementService;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import javax.transaction.Transactional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
public class AbonementTest {


    @Mock
    UserRepository userRepository;


    @Mock
    ReturnBookInput input;

    @Mock
    User user;
    @Mock
    Book book;
    @Mock
    Set<Abonnement> abonnementSet;

    @Captor
    ArgumentCaptor<Abonnement> abonnementArgumentCaptor;

    @Mock
    BookRepository bookRepository;

    @InjectMocks
    AbonnementService abonnementService;




    @Test
    @Rollback(false)
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    void returnBook() {
        Mockito.when(bookRepository.getReferenceById(any())).thenReturn(book);
        Mockito.when(book.getAmount()).thenReturn(0);
        Mockito.when(user.getAbonnements()).thenReturn(abonnementSet);
        Mockito.when(input.getStatus()).thenReturn("IN_USE");
        Mockito.when(input.getBookId()).thenReturn(4);



        abonnementService.returnBook(input, user);
        verify(bookRepository, times(1)).getReferenceById(4);
        Mockito.verify(book, times(1)).setAmount(1);
        Mockito.verify(abonnementSet).remove(abonnementArgumentCaptor.capture());

    }

    @Test
    @Rollback(false)
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    void returnBookWithFine() {
        Mockito.when(bookRepository.getReferenceById(any())).thenReturn(book);
        Mockito.when(book.getAmount()).thenReturn(0);
        Mockito.when(user.getAbonnements()).thenReturn(abonnementSet);
        Mockito.when(input.getStatus()).thenReturn("EXPIRED");
        Mockito.when(input.getBookId()).thenReturn(4);




        abonnementService.returnBook(input, user);
        verify(bookRepository, times(1)).getReferenceById(4);
        Mockito.verify(book, times(1)).setAmount(1);
        Mockito.verify(abonnementSet).remove(abonnementArgumentCaptor.capture());

    }
}
