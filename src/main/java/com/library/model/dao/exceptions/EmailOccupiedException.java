package com.library.model.dao.exceptions;

import com.library.model.dao.entity.User;

public class EmailOccupiedException extends RuntimeException {

    private User user;

    public EmailOccupiedException(Throwable cause, User user) {
        super(cause);
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
