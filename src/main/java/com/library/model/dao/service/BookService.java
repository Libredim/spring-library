package com.library.model.dao.service;

import com.library.controller.dto.BookDetailInput;
import com.library.controller.dto.BookInput;
import com.library.controller.dto.CatalogPageInput;
import com.library.model.dao.entity.Book;
import com.library.model.dao.jpa.AuthorRepository;
import com.library.model.dao.jpa.BookRepository;
import com.library.model.dao.jpa.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private AuthorRepository authorRepository;
    @Autowired
    private PublisherRepository publisherRepository;

@Transactional
    public void deleteBook(BookDetailInput input) {
        bookRepository.delete(new Book.Builder().setId(input.getBookId()).build());
    }
@Transactional
    public Optional<Book> editBook(BookInput input) {
        Book book=bookRepository.getReferenceById(input.getId());
        book.setName(input.getName());
        book.setAuthor(authorRepository.getReferenceById(input.getAuthor()));
        book.setAmount(input.getAmount());
        book.setPublisher(publisherRepository.getReferenceById(input.getPublisher()));

        book.setPublicationDate(extractDate(input.getDate()));

        return Optional.of(book);
    }

    public Optional<Book> createBook(BookInput input) {

        Book book=new Book();
        book.setName(input.getName());
        book.setAuthor(authorRepository.getReferenceById(input.getAuthor()));
        book.setAmount(input.getAmount());
        book.setPublisher(publisherRepository.getReferenceById(input.getPublisher()));
        DateFormat format=new SimpleDateFormat("yyyy-MM-dd");

        Date date = null;
        try {
            date = format.parse(input.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        book.setPublicationDate(date);
        bookRepository.save(book);
        return Optional.of(book);
    }

    public Optional<Set<Book>> search(String search) {
        Set<Book> books=new HashSet<>();
        books.addAll(bookRepository.findAllByName(search));

        return Optional.ofNullable(books);
    }



    public Optional<Book> findBook(BookDetailInput input) {
       return bookRepository.findById(input.getBookId());

    }

    public Page<Book> findPageOfBooks(CatalogPageInput input) {
        Pageable sortedBy;
        if(input.getDirection().equals("desc")) {
             sortedBy =
                    PageRequest.of(input.getPage() - 1, input.getBooksOnPage(), Sort.by(input.getKey()).descending());
        }else{
            sortedBy=PageRequest.of(input.getPage()-1,input.getBooksOnPage(),Sort.by(input.getKey()).ascending());
        }

        return bookRepository.findAll(sortedBy);

    }


    private Date extractDate(String str) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = dateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
