package com.library.model.dao.service;

import com.library.controller.dto.OrderInput;
import com.library.model.dao.entity.*;
import com.library.model.dao.jpa.AbonnementRepository;
import com.library.model.dao.jpa.BookRepository;
import com.library.model.dao.jpa.OrderRepository;
import com.library.model.dao.jpa.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Component
public class OrderService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    AbonnementRepository abonnementRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    BookRepository bookRepository;


    private int maxPage;

    public Page<Order> findPageOfOrders(int page, int ordersOnPage) {
        Pageable sortedByName =
                PageRequest.of(page - 1, ordersOnPage, Sort.by("user"));


        return orderRepository.findAll(sortedByName);
    }

    @Transactional
    public void approveOrder(OrderInput input) {
        System.out.println(input.getId());
        Optional<Order> opt = orderRepository.findById(input.getId());
        System.out.println(opt.isPresent());
        if (opt.isPresent()) {
            Order order = opt.get();
            if (order.getToAbonnement()) {
                approveToAbonement(order);
            } else {
                approveToReadingRoom(order);
            }
        }
    }

    @Transactional
    public void approveToAbonement(Order order) {
        User user = order.getUser();
        Book book = order.getBook();
        Abonnement abonnement = new Abonnement();

        book.setAmount(book.getAmount() - 1);
        abonnement.setId(new Abonnement.PK(user.getId(), book.getId()));
        abonnement.setUser(user);
        abonnement.setBook(book);
        abonnement.setAbonnementDetails(new AbonnementDetails.Builder()
                .setStartDate(new Date())
                .setExpDate(order.getDesiredDate())
                .setStatus(Status.IN_USE)
                .setFine(100.5)
                .build()
        );
        user.getOrders().remove(order);
        user.addAbonement(abonnement);
        book.getOrders().remove(order);
        book.addAbonement(abonnement);

        orderRepository.delete(order);
    }

    @Transactional
    public void approveToReadingRoom(Order order) {
        User user = order.getUser();
        Book book = order.getBook();


        book.setAmount(book.getAmount() - 1);
        user.setBookInReadingRoom(book);

        orderRepository.delete(order);

    }

    @Transactional
    public Order createOrder(OrderInput input, User user) {
        Book book = bookRepository.getReferenceById(input.getBookId());
        Order order = new Order.Builder()
                .setBook(book)
                .setUser(user)
                .setToAbonnement(input.getToAbonement())
                .setDesiredDate(input.getDesiredDate())
                .build();
        orderRepository.save(order);
        user.addOrder(order);
        book.addOrder(order);
        return order;
    }

    private Date extractDate(String str) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = dateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
