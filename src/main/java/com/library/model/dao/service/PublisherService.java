package com.library.model.dao.service;

import com.library.model.dao.entity.Publisher;
import com.library.model.dao.jpa.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
@Component
public class PublisherService {
@Autowired
    PublisherRepository publisherRepository;

    public Optional<List<Publisher>> findAll() {

        return Optional.of(publisherRepository.findAll());
    }





}
