package com.library.model.dao.service;

import com.library.controller.dto.ReturnBookInput;
import com.library.model.dao.entity.Abonnement;
import com.library.model.dao.entity.Book;
import com.library.model.dao.entity.Status;
import com.library.model.dao.entity.User;
import com.library.model.dao.jpa.BookRepository;
import com.library.model.dao.jpa.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

@Component
public class AbonnementService {

    @Autowired
    BookRepository bookRepository;
    @Autowired
    UserRepository userRepository;

    @Transactional
    public void returnBook(ReturnBookInput input, User user) {


        if (input.getStatus()==null) {
            returnBookFromReadingRoom(user);
        } else
        if (input.getStatus().equals(Status.IN_USE.name())) {
            returnBookInTime(input, user);
        } else if (input.getStatus().equals(Status.EXPIRED.name())) {
            returnWithFine(user, input);
        }


    }

    @Transactional
    private void returnBookFromReadingRoom(User user) {
        Book book = user.getBookInReadingRoom();

        user.setBookInReadingRoom(null);
        book.setAmount(book.getAmount() + 1);
        userRepository.save(user);
    }


    @Transactional
    private void returnBookInTime(ReturnBookInput input, User user) {
//        TODO proper exception

        Set<Abonnement> abonnementSet = user.getAbonnements();
        Book book = bookRepository.getReferenceById(input.getBookId());

        Abonnement entry = new Abonnement(user, book);
        abonnementSet.remove(entry);
        book.getAbonements().remove(entry);
        book.setAmount(book.getAmount() + 1);

    }

    private void returnWithFine(User user, ReturnBookInput input) {
        //        TODO proper exception

        Set<Abonnement> abonnementSet = user.getAbonnements();
        Book book = bookRepository.getReferenceById(input.getBookId());
        Abonnement entry = new Abonnement(user, book);
        book.setAmount(book.getAmount() + 1);

        abonnementSet.remove(entry);
        book.getAbonements().remove(entry);
        user.setAccount(user.getAccount() - input.getFine());

    }


    public Optional<Set<Abonnement>> getByUser(User user) {


        return Optional.ofNullable(user.getAbonnements());

    }


}
