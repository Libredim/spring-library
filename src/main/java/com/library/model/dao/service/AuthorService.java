package com.library.model.dao.service;

import com.library.controller.dto.AuthorInput;
import com.library.model.dao.entity.Author;
import com.library.model.dao.jpa.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
@Component
public class AuthorService {
    @Autowired
    private AuthorRepository authorRepository;
    @Transactional
    public void addAuthor(AuthorInput input) {

        authorRepository.save(new Author.Builder().setName(input.getName()).build());


    }

    public Optional<List<Author>> findAll() {

        return Optional.of(authorRepository.findAll());
    }





}
