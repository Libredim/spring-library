package com.library.model.dao.service;

import com.library.controller.dto.*;
import com.library.model.dao.entity.User;
import com.library.model.dao.entity.UserRole;
import com.library.model.dao.jpa.UserRepository;
import com.library.model.dao.jpa.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;


    public int getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }


    private int maxPage;

    @Transactional
    public void userTopUp(TopUpInput input, User user) {
        user.setAccount(user.getAccount() + input.getMoney());
    }

    @Transactional
    public void banUser(BanInput input) {
        User user=userRepository.getReferenceById(input.getUserId());
        user.setBan(true);
    }
    @Transactional
    public void unbanUser(BanInput input) {
        User user=userRepository.getReferenceById(input.getUserId());
        user.setBan(false);
    }
    @Transactional
    public void deleteUser(Integer userId) {
       userRepository.delete(userRepository.getReferenceById(userId));
    }



    public Optional<User> findUser(SignInInput input) {
       return userRepository.findByMail(input.getMail());

    }

    public Page<User> findPageOfUsers(UsersPage input) {
        Pageable sortedByName =
                PageRequest.of(input.getPage()-1, input.getUsersOnPage(), Sort.by("username"));


        return userRepository.findAll(sortedByName);

    }

    public boolean checkPassword(User user, String password) {
        return password.equals(user.getPassword());

    }
@Transactional
    public User saveUser(SubscribeInput input, UserRole role) {
        User user=new User.Builder()
                .setUsername(input.getUsername())
                .setPassword(input.getPasswordFirst())
                .setRole(role)
                .setMail(input.getMail())

        .build();
        userRepository.save(user);
        return user;
    }






}
