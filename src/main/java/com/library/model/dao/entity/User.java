package com.library.model.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String username;
    @Column(name = "email")
    private String mail;
    private String password;
    @Column(name = "create_time", insertable = false, updatable = false)
    private Date createDate;

    @ManyToMany
    @JoinTable(
            name = "user_has_role",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")}
    )
    private Set<UserRole> userRoles = new HashSet<>();

    @OneToMany( mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
    private Set<Order> orders;

    @Transient
    private Set<String> role = new HashSet<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Abonnement> abonnements = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinTable(
            name = "user_has_book",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "book_id")}
    )


    private Book bookInReadingRoom;
    private boolean ban;
    private double account;

    @PostLoad
    private void init() {
        if (userRoles != null) {
            role = userRoles.stream().map(UserRole::getName).collect(Collectors.toSet());
        }
    }

    public void addRole(UserRole role) {
        this.userRoles.add(role);
        this.role.add(role.getName());
    }

    public void addAbonement(Abonnement abonnement) {
        this.abonnements.add(abonnement);
    }
    public void addOrder(Order order){this.orders.add(order);}
    public void addBook(Book book) {
        Abonnement abonnement = new Abonnement(this, book);
        addAbonement(abonnement);
    }

    public static class Builder {
        private User instance = new User();

        public Builder setId(int id) {
            instance.id = id;
            return this;
        }

        public Builder setUsername(String username) {
            instance.username = username;
            return this;
        }

        public Builder setMail(String mail) {
            instance.mail = mail;
            return this;
        }

        public Builder setPassword(String password) {
            instance.password = password;
            return this;
        }

        public Builder setRole(UserRole role) {
            instance.addRole(role);
            return this;
        }

        public Builder setCreateDate(Date createDate) {
            instance.createDate = createDate;
            return this;
        }

        public Builder addAbonement(Abonnement abonnement) {
            instance.addAbonement(abonnement);
            return this;
        }

        public Builder setAccount(double account) {
            instance.account = account;
            return this;
        }

        public Builder setBan(boolean ban) {
            instance.ban = ban;
            return this;
        }

        public Builder setBookInReadingRoom(int id) {
            if (id < 1) instance.bookInReadingRoom = null;
            else instance.bookInReadingRoom = new Book.Builder().setId(id).build();
            return this;
        }


        public User build() {
            return instance;
        }
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + mail + '\'' +
                ", password='" + password + '\'' +
                ", createDate=" + createDate +
                ", role=" + role +
                ", abonnement=" + abonnements +
                ", book=" + bookInReadingRoom +
                ", account " + account +
                '}';
    }
}
