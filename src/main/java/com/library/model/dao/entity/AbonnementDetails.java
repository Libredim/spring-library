package com.library.model.dao.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Date;

@Embeddable
@Data
public class AbonnementDetails implements Serializable {
    private Date startDate;
    private Date expDate;
    private Double fine;
    @Column(name = "Status_id")
    @Convert(converter = StatusConverter.class)
    private Status status;


    public static class Builder {
        private AbonnementDetails instance = new AbonnementDetails();



        public Builder setStartDate(Date date) {
            instance.startDate = date;
            return this;
        }

        public Builder setExpDate(Date date) {
            instance.expDate = date;
            return this;
        }

        public Builder setStatus(Status status) {
            instance.status = status;
            return this;
        }

        public Builder setFine(double fine) {
            instance.fine = fine;
            return this;
        }

        public AbonnementDetails build() {
            return instance;
        }
    }


}
