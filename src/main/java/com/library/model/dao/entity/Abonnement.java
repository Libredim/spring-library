package com.library.model.dao.entity;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "abonement_has_book")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Abonnement {

    @EmbeddedId
    private PK id;

    @MapsId("userId")
    @ManyToOne
    private User user;

    @MapsId("bookId")
    @ManyToOne
    private Book book;

    @Embedded
    private AbonnementDetails abonnementDetails;



    public Abonnement(User user, Book book) {
        this();
        this.user = user;
        this.book = book;
        id = new PK(user.getId(), book.getId());
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Abonnement that = (Abonnement) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Embeddable
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class PK implements Serializable {
        @Column(name = "user_id")
        private Integer userId;

        @Column(name = "book_id")
        private Integer bookId;
    }
}
