package com.library.model.dao.entity;

public enum Role {
    GUEST(UserRole.builder().id(1).name("GUEST").build()),
    USER(UserRole.builder().id(2).name("USER").build()),
    ADMIN(UserRole.builder().id(3).name("ADMIN").build()),
    LIBRARIAN(UserRole.builder().id(4).name("LIBRARIAN").build());
    private UserRole userRole;

    Role(UserRole userRole) {
        this.userRole = userRole;
    }

    public static Role getRole(int id) {
        switch (id) {

            case 2:
                return Role.USER;

            case 3:
                return Role.ADMIN;

            case 4:
                return Role.LIBRARIAN;

            default:
                return Role.GUEST;

        }


    }

    public UserRole getUserRole() {
        return userRole;
    }
}
