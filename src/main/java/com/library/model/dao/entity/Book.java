package com.library.model.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @ManyToOne
    @JoinColumn(name = "publisher_id")
    private Publisher publisher;
    private int amount;
    @Column(name = "publication_date")
    private Date publicationDate;

    @ManyToOne
    @JoinColumn(name = "Author_id")
    private Author author;

    @OneToMany(mappedBy = "book",  orphanRemoval = true)
    private Set<Abonnement> abonements;

    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
    private Set<Order> orders;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", nameUA='" + name + '\'' +
                ", publisher=" + publisher +
                ", amount=" + amount +
                ", publicationDate=" + publicationDate +

                ", author=" + author +
                '}';
    }

    public boolean addAbonement(Abonnement ad) {
        ad.setBook(this);
        return abonements.add(ad);
    }

    public void addOrder(Order order) {
        this.orders.add(order);
    }

    public static class Builder {
        private Book instance = new Book();

        public Builder setId(int id) {
            instance.id = id;
            return this;
        }

        public Builder setName(String name) {
            instance.name = name;
            return this;
        }

        //TODO: check
        public Builder setPublisher(int id, String name) {

            instance.publisher = new Publisher.Builder()
                    .setId(id)
                    .setName(name).build();

            return this;
        }

        public Builder setAmount(int amount) {
            instance.amount = amount;
            return this;
        }

        public Builder setPublicationDate(Date publicationDate) {
            instance.publicationDate = publicationDate;
            return this;
        }

        public Builder setAuthor(int id, String name) {
            {

                instance.setAuthor(new Author.Builder()
                        .setId(id)

                        .setName(name).build());

            }
            return this;
        }


        public Book build() {


            return instance;
        }
    }
}


