package com.library.model.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "receipt")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    User user;


    @ManyToOne
    @JoinColumn(name = "book_id")
    Book book;
    @Column(name = "to_abonement")
    Boolean toAbonnement;
    @Column(name = "desired_date")
    Date desiredDate;


    @Override
    public String toString() {
        return "Order{" +
                "user=" + user +
                ", book=" + book +
                ", desiredDate=" + desiredDate +
                '}';
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Order order = (Order) o;
        return id != null && Objects.equals(id, order.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public static class Builder {
        private Order instance = new Order();

        public Builder setId(Integer id)
        {
            instance.id=id;
            return this;
        }
        public Builder setUser(User user) {
            instance.user = user;
            return this;
        }

        public Builder setBook(Book book) {
            instance.book = book;
            return this;
        }
        public Builder setToAbonnement(Boolean toAbonnement) {
            instance.toAbonnement = toAbonnement;
            return this;
        }

        public Builder setDesiredDate(String date) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            Date desiredDate = null;
            try {
                desiredDate = dateFormat.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            instance.desiredDate = desiredDate;
            return this;
        }

        public Order build() {


            return instance;
        }

    }
}
