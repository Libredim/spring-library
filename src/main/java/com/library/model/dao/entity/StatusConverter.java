package com.library.model.dao.entity;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class StatusConverter implements AttributeConverter<Status, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Status attribute) {
        return attribute.ordinal() + 1;
    }

    @Override
    public Status convertToEntityAttribute(Integer dbData) {
        return Status.getStatus(dbData);
    }
}
