package com.library.model.dao.entity;

public enum Status {
    IN_USE, RETURNED, EXPIRED, RETURNED_WITH_FINE;

    public static Status getStatus(int id) {
        switch (id) {
            case 1:
                return Status.IN_USE;
            case 2:
                return Status.RETURNED;

            case 3:
                return Status.EXPIRED;

            case 4:
                return Status.RETURNED_WITH_FINE;

            default:
                return Status.EXPIRED;

        }
    }

}
