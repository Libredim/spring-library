package com.library.controller;

import com.library.controller.dto.ReturnBookInput;
import com.library.model.dao.entity.Abonnement;
import com.library.model.dao.entity.User;
import com.library.model.dao.service.AbonnementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import java.util.Set;

@Controller
public class AbonnementController {

    @Autowired
    AbonnementService abonnementService;

    @GetMapping("/abonement")
    public String getAbonnement(Model model, HttpSession httpSession) {
        User user = (User) httpSession.getAttribute("user");
        model.addAttribute("input", new ReturnBookInput());
        Set<Abonnement> abonnement = user.getAbonnements();

        model.addAttribute("abonnements", abonnement);
        return "abonement";
    }

    @PostMapping("/return_book")
    public String returnBook(Model model, HttpSession httpSession, ReturnBookInput input) {

        abonnementService.returnBook(input,(User) httpSession.getAttribute("user"));
        return "redirect:/abonement";


    }


}
