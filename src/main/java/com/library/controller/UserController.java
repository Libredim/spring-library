package com.library.controller;

import com.library.controller.dto.*;
import com.library.model.dao.entity.Order;
import com.library.model.dao.entity.Role;
import com.library.model.dao.entity.User;
import com.library.model.dao.service.AuthorService;
import com.library.model.dao.service.OrderService;
import com.library.model.dao.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    AuthorService authorService;
    @Autowired
    OrderService orderService;



    @GetMapping("/sign_in")
    public String getSignIn(Model model) {
        model.addAttribute("input", new SignInInput());
        return "sign_in";
    }
    @GetMapping("/profile")
    public String getProfile(Model model) {
        model.addAttribute("input", new AuthorInput());
        return "profile";
    }
    @GetMapping("/top_up_page")
    public String getTopUpPage(Model model) {
        model.addAttribute("input", new TopUpInput());
        return "top_up_page";
    }
    @GetMapping("/orders")
    public String getOrders(Model model) {
        Page<Order> page=orderService.findPageOfOrders(1,9);
        List<Order> orders=page.stream().collect(Collectors.toList());
        model.addAttribute("orders",orders);
        model.addAttribute("input",new OrderInput());
        return "orders";
    }
    @GetMapping("/create_librarian_page")
    public String getCreateLibrarianPage(Model model) {
        model.addAttribute("input",new SubscribeInput());
        return "create_librarian_page";
    }
    @GetMapping("/users")
    public String getUsers(Model model) {
        UsersPage usersPage=new UsersPage();
        usersPage.setPage(1);
        usersPage.setUsersOnPage(5);
        Page<User> page=userService.findPageOfUsers(usersPage);
        List<User> list=page.toList();
        model.addAttribute("input",new BanInput());
        model.addAttribute("users",list);
        return "users";
    }
    @PostMapping("/create_author")
    public String CreateAuthor(@ModelAttribute @Valid AuthorInput input
            , BindingResult bindingResult, Model model, HttpSession httpSession) {
        if (bindingResult.hasErrors()) {
            return "redirect:/profile";
        }
        authorService.addAuthor(input);

        return "redirect:/profile";
    }
    @PostMapping("/top_up")
    public String TopUp(@ModelAttribute @Valid TopUpInput input
            , BindingResult bindingResult, Model model, HttpSession httpSession) {
        if (bindingResult.hasErrors()) {
            return "redirect:/profile";
        }
        User user= (User) httpSession.getAttribute("user");

userService.userTopUp(input,user);

        return "redirect:/profile";
    }

    @PostMapping("/sign_in")
    public String signIn(@ModelAttribute @Valid SignInInput input
            , BindingResult bindingResult, Model model, HttpSession httpSession) {
        if (bindingResult.hasErrors()) {
            return "redirect:/sign_in";
        }

        User user=userService.findUser(input).get();
        System.out.println(user);
        httpSession.setAttribute("user", user);
        return "redirect:/index";
    }

    @PostMapping("/logout")
    public String logout(Model model, HttpSession httpSession){
        httpSession.invalidate();
        return "redirect:/index";
    }
    @PostMapping("/approve_order")
    public String approveOrder(@ModelAttribute @Valid OrderInput input,BindingResult bindingResult,Model model, HttpSession httpSession){
        if (bindingResult.hasErrors()) {
            return "redirect:/sign_in";
        }
        orderService.approveOrder(input);
        return "redirect:/orders";
    }
    @PostMapping("/ban_user")
    public String banUser(@ModelAttribute  BanInput input,Model model, HttpSession httpSession){
        userService.banUser(input);
        return "redirect:/users";
    }
    @PostMapping("/unban_user")
    public String unbanUser(@ModelAttribute  BanInput input,Model model, HttpSession httpSession){
        userService.unbanUser(input);
        return "redirect:/users";
    }

    @GetMapping("/sign_up")
    public String getSignUp(Model model) {
        model.addAttribute("input", new SubscribeInput());

        return "sign_up";
    }

    @PostMapping("/create_librarian")
    public String createLibrarian(@ModelAttribute @Valid SubscribeInput input
            , BindingResult bindingResult, Model model, HttpSession httpSession) {
        if (bindingResult.hasErrors()) {
            return "redirect:/create_librarian_page";
        }
        userService.saveUser(input, Role.LIBRARIAN.getUserRole() );


        return "redirect:/profile";
    }
    @PostMapping("/sign_up")
    public String signUp(@ModelAttribute @Valid SubscribeInput input
            , BindingResult bindingResult, Model model, HttpSession httpSession) {
        if (bindingResult.hasErrors()) {
            return "redirect:/sign_up";
        }
        User user=userService.saveUser(input, Role.USER.getUserRole());
        System.out.println(user);

        return "redirect:/sign_in";
    }
}
