package com.library.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BookInput {
    private String name;
    private int id;
    private int publisher;
    private int amount;
    private int author;
    private String date;

    @Override
    public String toString() {
        return "BookInput{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", publisher=" + publisher +
                ", amount=" + amount +
                ", author=" + author +
                ", date='" + date + '\'' +
                '}';
    }



}
