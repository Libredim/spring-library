package com.library.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class OrderInput {
    Integer id;
    Integer userId;
    Integer bookId;
    Boolean toAbonement;
    String desiredDate;
}
