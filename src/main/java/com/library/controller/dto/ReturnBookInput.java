package com.library.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ReturnBookInput {
    int userId;
    int bookId;
    String expDate;
    String startDate;
    double fine;
    String status;

    @Override
    public String toString() {
        return "ReturnBookInput{" +
                "userId=" + userId +
                ", bookId=" + bookId +
                ", expDate='" + expDate + '\'' +
                ", startDate='" + startDate + '\'' +
                ", fine=" + fine +
                ", status='" + status + '\'' +
                '}';
    }

    public static class Builder {
        private ReturnBookInput instance = new ReturnBookInput();

        public Builder setUserId(int id) {
            instance.userId = id;
            return this;
        }

        public Builder setBookId(int id) {
            instance.bookId = id;
            return this;
        }

        public Builder setExpDate(String date) {

            instance.expDate = date;
            return this;
        }

        public Builder setStartDate(String date) {

            instance.startDate = date;
            return this;
        }

        public Builder setStatus(String status) {

            instance.status = status;
            return this;
        }

        public Builder setFine(String fineStr) {

            instance.fine = Double.parseDouble(fineStr);
            return this;
        }

        public ReturnBookInput build() {
            return instance;
        }
    }
}
