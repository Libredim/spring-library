package com.library.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@NoArgsConstructor

public class SubscribeInput {
    @NotBlank

    String username;
    @NotBlank
    @Email
    String mail;
    @NotBlank
    String passwordFirst;
    @NotBlank
    String passwordSecond;
    @Pattern(regexp = "GUEST|USER|ADMIN|LIBRARIAN")
    String role;

    public static class Builder {
        private SubscribeInput instance = new SubscribeInput();

        public Builder setUsername(String username) {
            instance.username = username;
            return this;
        }

        public Builder setFirstPassword(String password) {
            instance.passwordFirst = password;
            return this;
        }

        public Builder setSecondPassword(String password) {
            instance.passwordSecond = password;
            return this;
        }

        public Builder setMail(String mail) {
            instance.mail = mail;
            return this;
        }

        public Builder setRole(String role) {
            instance.role = role;
            return this;
        }

        public SubscribeInput build() {
            return instance;
        }

    }
}
