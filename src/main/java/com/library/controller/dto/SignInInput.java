package com.library.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
public class SignInInput {
    @NotBlank
    String mail;
    @NotBlank
    String password;
}
