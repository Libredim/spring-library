package com.library.controller.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class CatalogPageInput {
    int page;
    int booksOnPage;
    String key;
    String direction;


    public CatalogPageInput(){
        page=1;
        booksOnPage=3;
        key="name";
        direction="asc";
    }


    public static class Builder {
        private CatalogPageInput instance = new CatalogPageInput();

        public Builder setPage(String page) {
            if (page == null || page.isEmpty()) instance.page = 1;
            else instance.page = Integer.parseInt(page);

            return this;
        }

        public Builder setBooksOnPage(String booksOnPage) {
            if (booksOnPage == null || booksOnPage.isEmpty()) instance.booksOnPage = 3;
            else instance.booksOnPage = Integer.parseInt(booksOnPage);
            ;
            return this;
        }

        public Builder setKey(String key) {
            if (key == null || key.isEmpty()) key = "bookDirect";

            instance.key = key;
            return this;
        }

        public CatalogPageInput build() {
            return instance;
        }
    }
}
