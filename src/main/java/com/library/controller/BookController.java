package com.library.controller;

import com.library.controller.dto.BookDetailInput;
import com.library.controller.dto.BookInput;
import com.library.controller.dto.CatalogPageInput;
import com.library.controller.dto.OrderInput;
import com.library.model.dao.entity.Author;
import com.library.model.dao.entity.Book;
import com.library.model.dao.entity.Publisher;
import com.library.model.dao.entity.User;
import com.library.model.dao.service.AuthorService;
import com.library.model.dao.service.BookService;
import com.library.model.dao.service.OrderService;
import com.library.model.dao.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
public class BookController {
    @Autowired
    BookService bookService;
    @Autowired
    PublisherService publisherService;
    @Autowired
    OrderService orderService;
    @Autowired
    AuthorService authorService;

    @GetMapping("/create_book_page")
    public String getCreateBookPage(Model model) {
        model.addAttribute("input", new BookInput());
        List<Author> authors = authorService.findAll().get();
        List<Publisher> publishers = publisherService.findAll().get();
        model.addAttribute("authors", authors);
        model.addAttribute("publishers", publishers);
        return "create_book_page";
    }

    @GetMapping("/book_page")
    public String getBookPage(Model model, BookDetailInput bookDetail) {
        Book book = bookService.findBook(bookDetail).get();
        model.addAttribute("book", book);
        model.addAttribute("input", new OrderInput());

        return "book_page";
    }

    @GetMapping("/edit_book_page")
    public String getEditBookPage(Model model, BookDetailInput detailInput) {

        model.addAttribute("input", new BookInput());
        List<Author> authors = authorService.findAll().get();
        List<Publisher> publishers = publisherService.findAll().get();
        Book book=bookService.findBook(detailInput).get();

        model.addAttribute("book",book);
        model.addAttribute("authors", authors);
        model.addAttribute("publishers", publishers);
        return "edit_book_page";
    }

    @GetMapping("/catalog")
    public String getBooks(Model model,CatalogPageInput CatalogPage) {


        Page<Book> books=bookService.findPageOfBooks(CatalogPage);
        Integer previousPage=books.getNumber();

        model.addAttribute("books", books);
        model.addAttribute("bookDetail", new BookDetailInput());
        model.addAttribute("previousPage",previousPage);
        model.addAttribute("currentPage",previousPage+1);
        model.addAttribute("nextPage",previousPage+2);
        return "catalog";
    }

    @PostMapping("/create_book")
    public String createBook(@ModelAttribute @Valid BookInput input
            , BindingResult bindingResult, Model model, HttpSession httpSession) {
        if (bindingResult.hasErrors()) {
            return "redirect:/catalog";
        }

        bookService.createBook(input);

        return "redirect:/catalog";
    }

    @PostMapping("/edit_book")
    public String editBook(@ModelAttribute @Valid BookInput input
            , BindingResult bindingResult, Model model, HttpSession httpSession) {
        if (bindingResult.hasErrors()) {
            return "redirect:/catalog";
        }
        System.out.println(input.toString());
        bookService.editBook(input);

        return "redirect:/catalog";
    }

    @PostMapping("/delete_book")
    public String deleteBook(@ModelAttribute BookDetailInput input
            , Model model, HttpSession httpSession) {


        bookService.deleteBook(input);

        return "redirect:/catalog";
    }

    @PostMapping("/order_book")
    public String orderBook(@ModelAttribute  OrderInput input
            , Model model, HttpSession httpSession) {

        User user = (User) httpSession.getAttribute("user");
        orderService.createOrder( input, user);

        return "redirect:/catalog";
    }

}
